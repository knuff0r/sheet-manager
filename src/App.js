import React, {Component} from 'react';
import './App.css';
import Home from "./home/Home";

class App extends Component {

	constructor(props) {
		super(props);

		this.state = {
			sheets: [
				{
					_id: 1,
					title: "negus",
					composer: "himmler"
				},
				{
					_id: 2,
					title: "nigger",
					composer: "hittler"
				},
				{
					_id: 3,
					title: "himmler marsch",
					composer: "am ars"
				},
				{
					_id: 4,
					title: "negus",
					composer: "himmler"
				},
				{
					_id: 5,
					title: "nigger",
					composer: "hittler"
				},
				{
					_id: 6,
					title: "himmler marsch",
					composer: "am ars"
				},
				{
					_id: 7,
					title: "reichsnigger",
					composer: "hipel"
				},
				{
					_id: 8,
					title: "nigg2r",
					composer: "hims"
				},
				{
					_id: 9,
					title: "harunzwin marsch",
					composer: "ammasrianerars"
				},
			]
		}

		this.addSheet = this.addSheet.bind(this);
	}


	addSheet(sheet) {
		console.log("addiong sheet", sheet);
		this.setState(prevState => ({
			sheets: [...prevState.sheets, sheet]
		}));
	}

	render() {
		return (
			<div className="App">
				<Home sheets={this.state.sheets} addSheet={this.addSheet}/>
			</div>
		);
	}
}

export default App;
