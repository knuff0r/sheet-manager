import React, {Component} from 'react';
import './Home.css';

class Home extends Component {

	constructor(props) {
		super(props);

		this.state = {
			search: null,

			title: null,
			composer: null,
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmitSheet = this.handleSubmitSheet.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
		this.filterSheets = this.filterSheets.bind(this);
	}

	handleInputChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});
	}

	handleSubmitSheet(event) {
		this.props.addSheet({
			_id: 1,
			title: this.state.title,
			composer: this.state.composer,
		})
		event.preventDefault();
	}

	handleSearch(event) {
		const target = event.target;
		const value = target.value;

		this.setState({
			search: value
		});
	}

	filterSheets() {
		const search = this.state.search;
		let sheets = this.props.sheets;

		if (this.state.search !== null) {
			const terms = search.split(" ");
			for (let term of terms) {
				sheets = sheets.filter((sheet) =>
					sheet.title.includes(term) ||
					sheet.composer.includes(term)
				)
			}
		}

		return sheets;
	}


	render() {
		const sheets = this.filterSheets();
		return (
			<div className="Home">
				<div className="search">
					<input type="text" placeholder="Search Title/Composer/..." onChange={this.handleSearch}/>
				</div>

				<div className="sheet sheet-placeholder">
					<form className="sheet-form" onSubmit={this.handleSubmitSheet}>
						<div className="drop-or-upload">
							<div>Drop a File</div>
							<div>or</div>
							<div>Click to upload</div>
						</div>
						<div className="form-row">
							<label>Title:</label>
							<input name="title" type="text" value={this.state.title} onChange={this.handleInputChange}/>
						</div>
						<div className="form-row">
							<label>Composer:</label>
							<input name="composer" type="text" value={this.state.composer} onChange={this.handleInputChange}/>
						</div>
						<button type="submit">Save</button>
					</form>
				</div>

				<div className="sheets">
					{
						sheets.map(sheet =>
							<div className="sheet">{sheet.title} - {sheet.composer}</div>
						)
					}
				</div>

			</div>
		)
			;
	}
}

export default Home;
